import 'jodit/build/jodit.min.css'
import Vue from 'vue'
import JoditVue from 'jodit-vue'
if (process.client) {
  Vue.use(JoditVue)
}