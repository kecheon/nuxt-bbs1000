### What is nuxt-bbs1000
BBS module for nuxt

### Installation
1. cd to your nuxt main package
2. npm install --save nuxt-bbs1000
3. Enter following into nuxt.conf.js
```
    "modules": [
        ['nuxt-bbs1000', { namespace: 'bbs1000', API: 'your backend API' }]
    ]
```

### Usage
insert component BBS into your nuxt pages with title and collection as props
for example
```
<BBS :title="title" :collection="collection">
```

### Reference
This is a repo to accompany a tutorial on building a Nuxt module on [Medium](https://medium.com/carepenny/creating-a-nuxt-module-1c6e3cdf1037).