// store/modules/bbs1000.js
const article = function () {
  return {
    title: '',
    link: '',
    label: '',
    description: '',
    keywords: '',
    tags: '',
    body: '',
    author: '',
    id: null,
    published: false
  }
}

export default options => ({
  namespaced: true,
  state: () => ({
    options,
    article: article(),
    posts: [],
    count: options.initialValue
  }),
  mutations: {
    setArticle (state, article) {
      state.article = article
    },
    deleteArticle (state) {
      state.article = article()
    },
    setPosts (state, posts) {
      state.posts = posts
    },
    deletePosts (state) {
      state.posts = []
    },
    adjust(state, data) {
      state.count += data
    }
  },
  getters: {
    getUser: (state, getters, rootState) => {
      return rootState.users.user
    },
    getArticle: (state) => {
      return state.article
    },
    getPosts: (state) => {
      return state.posts
    },
    count: state => state.count
  },
  actions: {
    SET_ARTICLE({ commit }, payload) {
      commit('setArticle', payload)
    },
    DELETE_ARTICLE({ commit }) {
      commit('deleteArticle')
    },
    SET_POSTS({ commit }, payload) {
      commit('setPosts', payload)
    },
    DELETE_POSTS({ commit }) {
      commit('deletePosts')
    }
  }
})
