import bbs1000Module from './modules'
// get the options out using lodash templates; 
// options come from main package nuxt.config.js modules
// interpolate string 
const options = JSON.parse(`<%= JSON.stringify(options) %>`)
// extract the namespace var
const { namespace } = options
// create the plugin
export default ({ store }, inject) => {
  // register the module using namespace as the name.
  store.registerModule(namespace, bbs1000Module(options), {
    preserveState: Boolean(store.state[namespace]) // if the store module already exists, preserve it
  })
}
